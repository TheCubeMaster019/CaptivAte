#ifndef IFSFILE_H
#define IFSFILE_H

#include <SDL/SDL_rwops.h>

class IFSFile {
public:
    IFSFile(SDL_RWops* rw);
    SDL_RWops* GetRW();
    unsigned int Get2DXOffset();
    unsigned int Get1Offset();
    ~IFSFile();
private:
    SDL_RWops* file;
    unsigned int headerSize = 0;
    unsigned int iidxSize = 0;
};

#endif //IFSFILE_H
