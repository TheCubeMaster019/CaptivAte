#ifndef BEMANI2DX_H
#define BEMANI2DX_H

#include <SDL/SDL_rwops.h>
#include "audiohandler.h"

struct IIDXBlockHeader {
    char IIDX[4];
    unsigned int headerSize;
    unsigned int dataSize;
    unsigned short channels;
    unsigned short panning;
    unsigned short volume;
    unsigned int options;
};

class IIDXFile {
public:
    IIDXFile(SDL_RWops* rw, AudioHandler* a, unsigned int offset = 0);
    void Open();
    void PlaySample(unsigned int whatSample);
    ~IIDXFile();
private:
    AudioHandler* aud;
    SDL_RWops* file;
    char name[16];
    unsigned int headerSize;
    unsigned int numOfSamples;
    unsigned int* sampleLocations;
    IIDXBlockHeader* samples;
    int* volumes;
    unsigned int startOffset;
};

#endif //BEMANI2DX_H
