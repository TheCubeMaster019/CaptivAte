#include "timehandler.h"
#include <thread>

Timer::Timer(std::chrono::microseconds resolution) {
    duration = resolution;
}

void Timer::Start() {
    prevTime = std::chrono::steady_clock::now();
}

bool Timer::HasTicked() {
    currTime = std::chrono::steady_clock::now();
    if(currTime - prevTime >= duration) {
        prevTime += duration;
        return true;
    }
    return false;
}

void Timer::Sleep() {
    std::this_thread::sleep_for((duration - (std::chrono::steady_clock::now() - prevTime)) / 2);
}
