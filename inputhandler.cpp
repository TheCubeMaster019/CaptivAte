#include "inputhandler.h"

#include <iostream>

void InputHandler::SetKey(int e, KeyEvents event, unsigned int player) {
    CAKey k;
    k.event = event;
    k.sdlkey = e;
    k.player = player;
    k.state = KeyState::Off;
    k.cooldown = 0;
    keys.push_back(k);
}

void InputHandler::AddTT(unsigned int player)
{
    CATT tt;
    tt.player = player;
    tt.state = TTState::Stopped;
    tt.cooldown = 0;
    tts.push_back(tt);
}

void InputHandler::UpdateEvents() {
    SDL_Event e;
    while(SDL_PollEvent(&e)) {
        if(e.type == SDL_QUIT) {
            shouldClose = true;
        }
    }
    Uint8* keystates = SDL_GetKeyState(NULL);
    if(keystates[SDLK_ESCAPE]) {
        shouldClose = true;
    }
    for(unsigned int i = 0; i < keys.size(); i++) {
        if(keystates[keys[i].sdlkey] && keys[i].state == KeyState::Pressed) {
            //std::cout << "On!" << std::endl;
            keys[i].state = KeyState::On;
        }
        else if(keystates[keys[i].sdlkey] && keys[i].state != KeyState::On) {
            //std::cout << "Pressed!" << std::endl;
            keys[i].state = KeyState::Pressed;
        }

        if(!keystates[keys[i].sdlkey] && keys[i].state == KeyState::Released) {
            //std::cout << "Off!" << std::endl;
            keys[i].state = KeyState::Off;
        }
        else if(!keystates[keys[i].sdlkey] && keys[i].state != KeyState::Off) {
            //std::cout << "Released!" << std::endl;
            keys[i].state = KeyState::Released;
        }
    }
    UpdateTT();
}

void InputHandler::UpdateTT() {
    for(unsigned int i = 0; i < tts.size(); i++) {
        KeyState ttup = GetStateOfKey(KeyEvents::KeyTTUp, i + 1);
        KeyState ttdown = GetStateOfKey(KeyEvents::KeyTTDown, i + 1);
        if(ttup == KeyState::Pressed && ttdown != KeyState::Pressed) {
            tts[i].state = TTState::NowUp;
            //std::cout << "TT is now going up!" << std::endl;
        }
        else if(ttup == KeyState::On && tts[i].state == TTState::NowUp) {
            tts[i].state = TTState::Up;
            //std::cout << "TT is going up!" << std::endl;
        }

        if(ttdown == KeyState::Pressed && ttup != KeyState::Pressed) {
            tts[i].state = TTState::NowDown;
            //std::cout << "TT is now going down!" << std::endl;
        }
        else if(ttdown == KeyState::On && tts[i].state == TTState::NowDown) {
            tts[i].state = TTState::Down;
            //std::cout << "TT is going down!" << std::endl;
        }
        if(tts[i].state == TTState::Stopped) {
            continue;
        }
        if((ttup != KeyState::Pressed && ttup != KeyState::On) && (ttdown != KeyState::Pressed && ttdown != KeyState::On)) {
            tts[i].state = TTState::Stopped;
            //std::cout << "TT is stopped!" << std::endl;
        }
        else if(ttup == KeyState::Released && ttdown == KeyState::On) {
            if(tts[i].state == TTState::NowUp || tts[i].state == TTState::Up) {
                tts[i].state = TTState::Stopped;
                //std::cout << "TT is stopped!" << std::endl;
            }
        }
        else if(ttdown == KeyState::Released && ttup == KeyState::On) {
            if(tts[i].state == TTState::NowDown || tts[i].state == TTState::Down) {
                tts[i].state = TTState::Stopped;
                //std::cout << "TT is stopped!" << std::endl;
            }
        }
    }
}

bool InputHandler::GameShouldClose() {
    return shouldClose;
}

KeyState InputHandler::GetStateOfKey(KeyEvents event, unsigned int player) {
    for(unsigned int i = 0; i < keys.size(); i++) {
        if(keys[i].event == event && keys[i].player == player) {
            return keys[i].state;
        }
    }
    return KeyState::Off;
}

TTState InputHandler::GetStateOfTT(unsigned int player)
{
    for(unsigned int i = 0; i < tts.size(); i++) {
        if(tts[i].player == player) {
            return tts[i].state;
        }
    }
    return TTState::Stopped;
}
