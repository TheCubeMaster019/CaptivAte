#ifndef BEMANI1_H
#define BEMANI1_H

#include <vector>
#include <SDL/SDL_rwops.h>
#include "chart.h"

struct location_size {
    unsigned int location;
    unsigned int size;
};

class ONEFile {
public:
    ONEFile(SDL_RWops* rw, unsigned int offset = 0);
    Chart OpenChart(ChartDifficulties difficulty, unsigned int players);
    ~ONEFile();
private:
    location_size* locs;
    Chart* charts;
    unsigned int open_chart;
    SDL_RWops* file;
    unsigned int startOffset;
};

#endif //BEMANI1_H
