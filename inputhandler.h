#ifndef INPUTHANDLER_H
#define INPUTHANDLER_H

#include <SDL/SDL.h>
#include <vector>

#define KEY_COOLDOWN 30
#define TT_COOLDOWN 200

enum KeyEvents {
    Key1,
    Key2,
    Key3,
    Key4,
    Key5,
    Key6,
    Key7,
    KeyTTUp,
    KeyTTDown,
    KeyStart
};

enum KeyState {
    Off,
    On,
    Pressed,
    Released
};

enum TTState {
    Stopped,
    NowUp,
    Up,
    NowDown,
    Down
};

struct CAKey {
    KeyState state;
    KeyEvents event;
    int sdlkey;
    unsigned int player;
    unsigned int cooldown;
};

struct CATT {
    TTState state;
    unsigned int player;
    unsigned int cooldown;
};

class InputHandler {
public:
    void SetKey(int e, KeyEvents event, unsigned int player);
    void AddTT(unsigned int player);
    void UpdateEvents();
    bool GameShouldClose();
    KeyState GetStateOfKey(KeyEvents event, unsigned int player);
    TTState GetStateOfTT(unsigned int player);
private:
    void UpdateTT();
    std::vector<CAKey> keys;
    std::vector<CATT> tts;
    bool shouldClose = false;
};

#endif //INPUTHANDLER_H
