CFL=-Wall -std=c++11 -g -O3
LFL=-lSDL -lSDL_image -lSDL_mixer
SRC=main.cpp bemani1.cpp bemani2dx.cpp graphicshandler.cpp timehandler.cpp ifsfile.cpp inputhandler.cpp audiohandler.cpp player.cpp
OBJ=$(SRC:.cpp=.o)
EXE=CaptivAte

all: build

build:
	g++ $(CFL) -c $(SRC)
	g++ $(LFL) $(OBJ) -o $(EXE)

clean:
	rm $(OBJ) $(EXE)
