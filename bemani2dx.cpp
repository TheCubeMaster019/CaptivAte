#include "bemani2dx.h"
#include <cmath>

IIDXFile::IIDXFile(SDL_RWops* rw, AudioHandler* a, unsigned int offset) {
    aud = a;
    file = rw;
    startOffset = offset;
    volumes = new int[256];
    for(unsigned int i = 0; i < 256; i++) {
        volumes[i] = (int)(std::pow(10.0, (-36.0 * i / 64.0) / 20.0) * 0xFF);
    }
}

void IIDXFile::Open() {
    SDL_RWseek(file, startOffset, SEEK_SET);
    SDL_RWread(file, name, sizeof(char), 16);
    SDL_RWread(file, &headerSize, sizeof(unsigned int), 1);
    SDL_RWread(file, &numOfSamples, sizeof(unsigned int), 1);
    SDL_RWseek(file, 0x30, SEEK_CUR);
    Mix_AllocateChannels(numOfSamples);
    aud->SetNumOfSamples(numOfSamples);
    sampleLocations = new unsigned int[numOfSamples];
    for(unsigned int i = 0; i < numOfSamples; i++) {
        SDL_RWread(file, &sampleLocations[i], sizeof(unsigned int), 1);
        //fprintf(stdout, "%i\n", iidx.sampleLocations[i]);
    }
    samples = new IIDXBlockHeader[numOfSamples];
    for(unsigned int i = 0; i < numOfSamples; i++) {
        SDL_RWseek(file, sampleLocations[i] + startOffset, SEEK_SET);
        SDL_RWread(file, samples[i].IIDX, sizeof(char), 4);
        SDL_RWread(file, &samples[i].headerSize, sizeof(unsigned int), 1);
        SDL_RWread(file, &samples[i].dataSize, sizeof(unsigned int), 1);
        SDL_RWseek(file, sizeof(short), SEEK_CUR);
        SDL_RWread(file, &samples[i].channels, sizeof(unsigned short), 1);
        SDL_RWread(file, &samples[i].panning, sizeof(unsigned short), 1);
        SDL_RWread(file, &samples[i].volume, sizeof(unsigned short), 1);
        SDL_RWread(file, &samples[i].options, sizeof(unsigned int), 1);
        if(samples[i].volume > 0xFF)
            samples[i].volume = 0xFF;
        if(samples[i].panning > 0x80)
            samples[i].panning = 0x40;
        aud->AddChunk(Mix_LoadWAV_RW(file, SDL_FALSE), samples[i].panning, volumes[samples[i].volume]);
    }
}

IIDXFile::~IIDXFile() {
    if(startOffset == 0) {
        SDL_RWclose(file);
    }

    delete[] samples;
    delete[] sampleLocations;
    delete[] volumes;
}
