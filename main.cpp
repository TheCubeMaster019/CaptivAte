#include "bemani1.h"
#include "bemani2dx.h"
#include "timehandler.h"
#include "graphicshandler.h"
#include "ifsfile.h"
#include "player.h"
#include <thread>
#include <string>

void init() {
    SDL_Init(SDL_INIT_EVERYTHING);
}

void terminate() {
    SDL_Quit();
}

const char* get_file_extension(const char* filename) {
    std::string temp(filename);
    size_t i = temp.rfind('.', temp.length());
    if(i != std::string::npos) {
        const char* out = &temp.c_str()[i];
        return out;
    }

    return nullptr;
}

int main(int argc, char* argv[]) {
    if(argc < 2 || argc > 4) {
        fprintf(stderr, "Please check your arguments...\n");
        return 1;
    }

    init();

    AudioHandler audio;

    bool ifsExists = false, iidxExists = false, oneExists = false;
    IIDXFile* iidx = nullptr;
    ONEFile* one = nullptr;
    IFSFile* ifs = nullptr;

    for(int i = 1; i < argc; i++) {
        const char* offset = get_file_extension(argv[i]);
        if(strcmp(offset, ".ifs") == 0 && !iidxExists && !oneExists) {
            ifs = new IFSFile(SDL_RWFromFile(argv[i], "rb"));
            iidx = new IIDXFile(ifs->GetRW(), &audio, ifs->Get2DXOffset());
            one = new ONEFile(ifs->GetRW(), ifs->Get1Offset());
            ifsExists = true;
            iidxExists = true;
            oneExists = true;
        }
        if(!ifsExists) {
            if(strcmp(offset, ".2dx") == 0 && !iidxExists) {
                iidx = new IIDXFile(SDL_RWFromFile(argv[i], "rb"), &audio);
                iidxExists = true;
            }
            if(strcmp(offset, ".1") == 0 && !oneExists) {
                one = new ONEFile(SDL_RWFromFile(argv[i], "rb"));
                oneExists = true;
            }
        }
    }

    if(!iidxExists || !oneExists) {
        fprintf(stderr, "I need a .1 AND a .2dx file. Please. :\\\n");
        return 1;
    }

    GraphicsHandler gfx(SCREEN_WIDTH, SCREEN_HEIGHT);

    //LOAD THE 2DX FILE

    iidx->Open();

    //START THE GAME - W00T

    Player player1(one->OpenChart(ChartDifficulties::Hyper, 1), &gfx, &audio);

    bool quit = false;
    Timer audioTimer(std::chrono::microseconds(1001));
    Timer fpsTimer(std::chrono::microseconds((int)((float)1000000 / FPS)));
    audioTimer.Start();
    fpsTimer.Start();
    bool wait = true;
    //GAME LOOP
    while(!quit) {
        //GRAPHICS
        if(fpsTimer.HasTicked()) {
            player1.Render(SIDE_OFFSET, BOTTOM_OFFSET);

            gfx.UpdateScreen();
        }

        //AUDIO
        if(audioTimer.HasTicked()){
            player1.Update();

            //wait = true;
        }

        if(wait) {
            audioTimer.Sleep();
            wait = false;
        }

        if(player1.IsReadyToQuit() && quit == false) {
            quit = true;
        }
    }

    if(ifsExists) {
        delete ifs;
    }
    if (iidxExists) {
        delete iidx;
    }
    if (oneExists) {
        delete one;
    }

    terminate();

    return 0;
}
