#include "graphicshandler.h"

GraphicsHandler::GraphicsHandler(unsigned int width, unsigned int height) {
    IMG_Init(IMG_INIT_PNG);
    windowSurface = SDL_SetVideoMode(width, height, 32, SDL_SWSURFACE);
    w = width;
    h = height;
}

unsigned int GraphicsHandler::AddSurface(const char* path) {
    SDL_Surface* temp = nullptr,* final = nullptr;
    temp = IMG_Load(path);
    final = SDL_DisplayFormatAlpha(temp);
    SDL_FreeSurface(temp);
    surfaces.push_back(final);
    return surfaces.size();
}

void GraphicsHandler::Draw(unsigned int surface, short x, short y) {
    if(surface == 0 || surface > surfaces.size()) {
        return;
    }
    SDL_Rect r;
    r.x = x;
    r.y = y;
    SDL_BlitSurface(surfaces[--surface], nullptr, windowSurface, &r);
}

void GraphicsHandler::UpdateScreen() {
    SDL_Flip(windowSurface);
}

GraphicsHandler::~GraphicsHandler() {
    for(unsigned int i = surfaces.size(); i > 0; i--) {
        SDL_FreeSurface(surfaces[i - 1]);
        surfaces.pop_back();
    }

    IMG_Quit();
}
