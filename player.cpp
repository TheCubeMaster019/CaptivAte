#include "player.h"
#include <iostream>

Player::Player(Chart c, GraphicsHandler* g, AudioHandler* a) {
    gfx = g;
    aud = a;

    graphicsIndex[0] = gfx->AddSurface("graphics/track_1p.png");
    graphicsIndex[1] = gfx->AddSurface("graphics/note_tt.png");
    graphicsIndex[2] = gfx->AddSurface("graphics/note_1357.png");
    graphicsIndex[3] = gfx->AddSurface("graphics/note_246.png");
    graphicsIndex[4] = gfx->AddSurface("graphics/measure.png");
    graphicsIndex[5] = gfx->AddSurface("graphics/note_tt_charge_start.png");
    graphicsIndex[6] = gfx->AddSurface("graphics/note_tt_charge_end.png");
    graphicsIndex[7] = gfx->AddSurface("graphics/note_tt_charge_between.png");
    graphicsIndex[8] = gfx->AddSurface("graphics/note_1357_charge_start.png");
    graphicsIndex[9] = gfx->AddSurface("graphics/note_1357_charge_end.png");
    graphicsIndex[10] = gfx->AddSurface("graphics/note_1357_charge_between.png");
    graphicsIndex[11] = gfx->AddSurface("graphics/note_246_charge_start.png");
    graphicsIndex[12] = gfx->AddSurface("graphics/note_246_charge_end.png");
    graphicsIndex[13] = gfx->AddSurface("graphics/note_246_charge_between.png");

    input.SetKey(SDLK_a, KeyEvents::Key1, 1);
    input.SetKey(SDLK_w, KeyEvents::Key2, 1);
    input.SetKey(SDLK_s, KeyEvents::Key3, 1);
    input.SetKey(SDLK_e, KeyEvents::Key4, 1);
    input.SetKey(SDLK_d, KeyEvents::Key5, 1);
    input.SetKey(SDLK_r, KeyEvents::Key6, 1);
    input.SetKey(SDLK_f, KeyEvents::Key7, 1);
    input.SetKey(SDLK_LSHIFT, KeyEvents::KeyTTUp, 1);
    input.SetKey(SDLK_LCTRL, KeyEvents::KeyTTDown, 1);
    input.AddTT(1);

    chart = c;

    grooveGauge = 11;
    grooveCounter = 0;
    exScore = 0;
    numberOfNotes = 0;
    for(Note& note : chart.notes) {
        if(note.type == NoteTypes::NoteCount) {
            numberOfNotes += note.value;
        }
        if(note.type == NoteTypes::Marker) {
            note.playable = true;
        }
    }
    combo = 0;
    maxCombo = 0;
    currPos = -1;
    readyToQuit = false;
}

void Player::Update() {
    currPos++;
    input.UpdateEvents();

    std::vector<Note*> ns;

    for(Note& note : chart.notes) {
        if(note.offset + judgements[5] + 1 == (unsigned int)currPos && note.type == NoteTypes::Marker) {
            if(note.playable) {
                BreakCombo();
                std::cout << "Missed a note! ";
                if(note.parameter == 7) {
                    std::cout << "TT";
                } else {
                    std::cout << note.parameter + 1;
                }
                std::cout << std::endl;
            }
        }
        if(note.offset - -judgements[0] <= (unsigned int)currPos && note.offset + judgements[5] >= (unsigned int)currPos && note.type == NoteTypes::Marker) {
            ns.push_back(&note);
        }

        if(note.offset == (unsigned int)currPos) {
            switch(note.type) {
            case NoteTypes::BGA:
                aud->PlaySample(note.value);
                break;
            case NoteTypes::Sample:
                playback_sample[note.parameter + 8 * (note.player - 1)] = note.value;
                break;
            case NoteTypes::Judgement:
                judgements[note.parameter] = (char)(note.value & 0xFF) * (1000/JUDGE_FPS_CONST) - (500 / JUDGE_FPS_CONST);
                std::cout << "judgement " << (int)note.parameter << " is now " << (int)judgements[note.parameter] << std::endl;
                break;
            case NoteTypes::EndOfSong:
                readyToQuit = true;
                break;
            default:
                break;
            }
        }
    }

    if(input.GetStateOfKey(KeyEvents::Key1, 1) == KeyState::Pressed) {
        aud->PlaySample(playback_sample[0]);
        bool hitANote = false;
        bool noteInLane = false;
        for(Note* note : ns) {
            if(note->parameter == 0) {
                noteInLane = true;
                if(note->playable) {
                    note->playable = false;
                    hitANote = true;
                    if(note->offset - -judgements[1] > (unsigned int)currPos || note->offset + judgements[4] < (unsigned int)currPos) {
                        BreakCombo();
                        std::cout << "Bad! 1" << std::endl;
                    } else {
                        IncreaseCombo();
                        if(note->offset - -judgements[2] > (unsigned int)currPos || note->offset + judgements[3] < (unsigned int)currPos) {
                            exScore += 1;
                            std::cout << "Good! 1" << std::endl;
                        } else if(note->offset - - (0 * 1000 / JUDGE_FPS_CONST) - (500 / JUDGE_FPS_CONST) > (unsigned int)currPos || note->offset + (1 * 1000/JUDGE_FPS_CONST) - (500 / JUDGE_FPS_CONST) < (unsigned int)currPos) {
                            exScore += 1;
                            std::cout << "Great! 1" << std::endl;
                        } else {
                            exScore += 2;
                            std::cout << "Just Great! 1" << std::endl;
                        }
                    }
                }
            }
            if(hitANote) {
                break;
            }
        }
        if(!hitANote && noteInLane) {
            BreakCombo();
            std::cout << "Pressed too much! 1" << std::endl;
        }
    }
    if(input.GetStateOfKey(KeyEvents::Key2, 1) == KeyState::Pressed) {
        aud->PlaySample(playback_sample[1]);
        bool hitANote = false;
        bool noteInLane = false;
        for(Note* note : ns) {
            if(note->parameter == 1) {
                noteInLane = true;
                if(note->playable) {
                    note->playable = false;
                    hitANote = true;
                    if(note->offset - -judgements[1] > (unsigned int)currPos || note->offset + judgements[4] < (unsigned int)currPos) {
                        BreakCombo();
                        std::cout << "Bad! 2" << std::endl;
                    } else {
                        IncreaseCombo();
                        if(note->offset - -judgements[2] > (unsigned int)currPos || note->offset + judgements[3] < (unsigned int)currPos) {
                            exScore += 1;
                            std::cout << "Good! 2" << std::endl;
                        } else if(note->offset > (unsigned int)currPos || note->offset + 1 < (unsigned int)currPos) {
                            exScore += 1;
                            std::cout << "Great! 2" << std::endl;
                        } else {
                            exScore += 2;
                            std::cout << "Just Great! 2" << std::endl;
                        }
                    }
                }
            }
            if(hitANote) {
                break;
            }
        }
        if(!hitANote && noteInLane) {
            BreakCombo();
            std::cout << "Pressed too much! 2" << std::endl;
        }
    }
    if(input.GetStateOfKey(KeyEvents::Key3, 1) == KeyState::Pressed) {
        aud->PlaySample(playback_sample[2]);
        bool hitANote = false;
        bool noteInLane = false;
        for(Note* note : ns) {
            if(note->parameter == 2) {
                noteInLane = true;
                if(note->playable) {
                    note->playable = false;
                    hitANote = true;
                    if(note->offset - -judgements[1] > (unsigned int)currPos || note->offset + judgements[4] < (unsigned int)currPos) {
                        BreakCombo();
                        std::cout << "Bad! 3" << std::endl;
                    } else {
                        IncreaseCombo();
                        if(note->offset - -judgements[2] > (unsigned int)currPos || note->offset + judgements[3] < (unsigned int)currPos) {
                            exScore += 1;
                            std::cout << "Good! 3" << std::endl;
                        } else if(note->offset > (unsigned int)currPos || note->offset + 1 < (unsigned int)currPos) {
                            exScore += 1;
                            std::cout << "Great! 3" << std::endl;
                        } else {
                            exScore += 2;
                            std::cout << "Just Great! 3" << std::endl;
                        }
                    }
                }
            }
            if(hitANote) {
                break;
            }
        }
        if(!hitANote && noteInLane) {
            BreakCombo();
            std::cout << "Pressed too much! 3" << std::endl;
        }
    }
    if(input.GetStateOfKey(KeyEvents::Key4, 1) == KeyState::Pressed) {
        aud->PlaySample(playback_sample[3]);
        bool hitANote = false;
        bool noteInLane = false;
        for(Note* note : ns) {
            if(note->parameter == 3) {
                noteInLane = true;
                if(note->playable) {
                    note->playable = false;
                    hitANote = true;
                    if(note->offset - -judgements[1] > (unsigned int)currPos || note->offset + judgements[4] < (unsigned int)currPos) {
                        BreakCombo();
                        std::cout << "Bad! 4" << std::endl;
                    } else {
                        IncreaseCombo();
                        if(note->offset - -judgements[2] > (unsigned int)currPos || note->offset + judgements[3] < (unsigned int)currPos) {
                            exScore += 1;
                            std::cout << "Good! 4" << std::endl;
                        } else if(note->offset > (unsigned int)currPos || note->offset + 1 < (unsigned int)currPos) {
                            exScore += 1;
                            std::cout << "Great! 4" << std::endl;
                        } else {
                            exScore += 2;
                            std::cout << "Just Great! 4" << std::endl;
                        }
                    }
                }
            }
            if(hitANote) {
                break;
            }
        }
        if(!hitANote && noteInLane) {
            BreakCombo();
            std::cout << "Pressed too much! 4" << std::endl;
        }
    }
    if(input.GetStateOfKey(KeyEvents::Key5, 1) == KeyState::Pressed) {
        aud->PlaySample(playback_sample[4]);
        bool hitANote = false;
        bool noteInLane = false;
        for(Note* note : ns) {
            if(note->parameter == 4) {
                noteInLane = true;
                if(note->playable) {
                    note->playable = false;
                    hitANote = true;
                    if(note->offset - -judgements[1] > (unsigned int)currPos || note->offset + judgements[4] < (unsigned int)currPos) {
                        BreakCombo();
                        std::cout << "Bad! 5" << std::endl;
                    } else {
                        IncreaseCombo();
                        if(note->offset - -judgements[2] > (unsigned int)currPos || note->offset + judgements[3] < (unsigned int)currPos) {
                            exScore += 1;
                            std::cout << "Good! 5" << std::endl;
                        } else if(note->offset > (unsigned int)currPos || note->offset + 1 < (unsigned int)currPos) {
                            exScore += 1;
                            std::cout << "Great! 5" << std::endl;
                        } else {
                            exScore += 2;
                            std::cout << "Just Great! 5" << std::endl;
                        }
                    }
                }
            }
            if(hitANote) {
                break;
            }
        }
        if(!hitANote && noteInLane) {
            BreakCombo();
            std::cout << "Pressed too much! 5" << std::endl;
        }
    }
    if(input.GetStateOfKey(KeyEvents::Key6, 1) == KeyState::Pressed) {
        aud->PlaySample(playback_sample[5]);
        bool hitANote = false;
        bool noteInLane = false;
        for(Note* note : ns) {
            if(note->parameter == 5) {
                noteInLane = true;
                if(note->playable) {
                    note->playable = false;
                    hitANote = true;
                    if(note->offset - -judgements[1] > (unsigned int)currPos || note->offset + judgements[4] < (unsigned int)currPos) {
                        BreakCombo();
                        std::cout << "Bad! 6" << std::endl;
                    } else {
                        IncreaseCombo();
                        if(note->offset - -judgements[2] > (unsigned int)currPos || note->offset + judgements[3] < (unsigned int)currPos) {
                            exScore += 1;
                            std::cout << "Good! 6" << std::endl;
                        } else if(note->offset > (unsigned int)currPos || note->offset + 1 < (unsigned int)currPos) {
                            exScore += 1;
                            std::cout << "Great! 6" << std::endl;
                        } else {
                            exScore += 2;
                            std::cout << "Just Great! 6" << std::endl;
                        }
                    }
                }
            }
            if(hitANote) {
                break;
            }
        }
        if(!hitANote && noteInLane) {
            BreakCombo();
            std::cout << "Pressed too much! 6" << std::endl;
        }
    }
    if(input.GetStateOfKey(KeyEvents::Key7, 1) == KeyState::Pressed) {
        aud->PlaySample(playback_sample[6]);
        bool hitANote = false;
        bool noteInLane = false;
        for(Note* note : ns) {
            if(note->parameter == 6) {
                noteInLane = true;
                if(note->playable) {
                    note->playable = false;
                    hitANote = true;
                    if(note->offset - -judgements[1] > (unsigned int)currPos || note->offset + judgements[4] < (unsigned int)currPos) {
                        BreakCombo();
                        std::cout << "Bad! 7" << std::endl;
                    } else {
                        IncreaseCombo();
                        if(note->offset - -judgements[2] > (unsigned int)currPos || note->offset + judgements[3] < (unsigned int)currPos) {
                            exScore += 1;
                            std::cout << "Good! 7" << std::endl;
                        } else if(note->offset > (unsigned int)currPos || note->offset + 1 < (unsigned int)currPos) {
                            exScore += 1;
                            std::cout << "Great! 7" << std::endl;
                        } else {
                            exScore += 2;
                            std::cout << "Just Great! 7" << std::endl;
                        }
                    }
                }
            }
            if(hitANote) {
                break;
            }
        }
        if(!hitANote && noteInLane) {
            BreakCombo();
            std::cout << "Pressed too much! 7" << std::endl;
        }
    }
    if(input.GetStateOfTT(1) == TTState::NowUp || input.GetStateOfTT(1) == TTState::NowDown) {
        aud->PlaySample(playback_sample[7]);
        bool hitANote = false;
        bool noteInLane = false;
        for(Note* note : ns) {
            if(note->parameter == 7) {
                noteInLane = true;
                if(note->playable) {
                    note->playable = false;
                    hitANote = true;
                    if(note->offset - -judgements[1] > (unsigned int)currPos || note->offset + judgements[4] < (unsigned int)currPos) {
                        BreakCombo();
                        std::cout << "Bad! TT" << std::endl;
                    } else {
                        IncreaseCombo();
                        if(note->offset - -judgements[2] > (unsigned int)currPos || note->offset + judgements[3] < (unsigned int)currPos) {
                            exScore += 1;
                            std::cout << "Good! TT" << std::endl;
                        } else if(note->offset > (unsigned int)currPos || note->offset + 1 < (unsigned int)currPos) {
                            exScore += 1;
                            std::cout << "Great! TT" << std::endl;
                        } else {
                            exScore += 2;
                            std::cout << "Just Great! TT" << std::endl;
                        }
                    }
                }
            }
            if(hitANote) {
                break;
            }
        }
        if(!hitANote && noteInLane) {
            BreakCombo();
            std::cout << "Pressed too much! TT" << std::endl;
        }
    }
}

void Player::Render(int x, int y) {
    std::vector<Note> notes;
    for(Note& note : chart.notes) {
        if(note.offset >= (unsigned int)currPos && note.offset <= currPos + SCREEN_HEIGHT - y) {
            notes.push_back(note);
        }
        else if(note.type == NoteTypes::Marker && note.offset + note.value >= (unsigned int)currPos && note.offset < (unsigned int)currPos) {
            notes.push_back(note);
        }
    }

    //Draw player 1 track
    for(unsigned int i = 0; i < SCREEN_HEIGHT - y; i++) {
        gfx->Draw(graphicsIndex[0], x, i);
    }

    //Draw the measure lines
    for(unsigned int i = 0; i < notes.size(); i++) {
        if(notes[i].type != NoteTypes::Measure || notes[i].player != 1) {
            continue;
        }
        short tempy = (-(notes[i].offset - currPos - SCREEN_HEIGHT) - 2) - y;
        gfx->Draw(graphicsIndex[4], x, tempy);
    }

    //Draw player 1 notes
    for(unsigned int i = 0; i < notes.size(); i++) {
        if(notes[i].type != NoteTypes::Marker || notes[i].player != 1) {
            continue;
        }
        if(notes[i].value == 0) { //NOT A CHARGE NOTE
            short tempy = (-(notes[i].offset - currPos - SCREEN_HEIGHT) - 8) - y;
            switch(notes[i].parameter) {
            case 0:
                gfx->Draw(graphicsIndex[2], 62 + x, tempy);
                break;
            case 1:
                gfx->Draw(graphicsIndex[3], 98 + x, tempy);
                break;
            case 2:
                gfx->Draw(graphicsIndex[2], 126 + x, tempy);
                break;
            case 3:
                gfx->Draw(graphicsIndex[3], 162 + x, tempy);
                break;
            case 4:
                gfx->Draw(graphicsIndex[2], 190 + x, tempy);
                break;
            case 5:
                gfx->Draw(graphicsIndex[3], 226 + x, tempy);
                break;
            case 6:
                gfx->Draw(graphicsIndex[2], 254 + x, tempy);
                break;
            case 7:
                gfx->Draw(graphicsIndex[1], 0 + x, tempy);
                break;
            }
        }
        else { //CHARGE NOTE
            short starty = (-(notes[i].offset - currPos - SCREEN_HEIGHT) - 8) - y;
            short endy = starty - notes[i].value - 8;
            switch(notes[i].parameter) {
            case 0:
                if(notes[i].offset >= (unsigned int)currPos)
                    gfx->Draw(graphicsIndex[8], 62 + x, starty);
                gfx->Draw(graphicsIndex[9], 62 + x, endy);
                for(unsigned short a = 0; a < notes[i].value; a++) {
                    if(notes[i].offset + a + 8 >= (unsigned int)currPos)
                        gfx->Draw(graphicsIndex[10], 62 + x, starty - a - 8 - 1);
                }
                break;
            case 1:
                if(notes[i].offset >= (unsigned int)currPos)
                    gfx->Draw(graphicsIndex[11], 98 + x, starty);
                gfx->Draw(graphicsIndex[12], 98 + x, endy);
                for(unsigned short a = 0; a < notes[i].value - 8; a++) {
                    if(notes[i].offset + a + 16 >= (unsigned int)currPos)
                        gfx->Draw(graphicsIndex[13], 98 + x, starty - a - 8 - 1);
                }
                break;
            case 2:
                if(notes[i].offset >= (unsigned int)currPos)
                    gfx->Draw(graphicsIndex[8], 126 + x, starty);
                gfx->Draw(graphicsIndex[9], 126 + x, endy);
                for(unsigned short a = 0; a < notes[i].value - 8; a++) {
                    if(notes[i].offset + a + 16 >= (unsigned int)currPos)
                        gfx->Draw(graphicsIndex[10], 126 + x, starty - a - 8 - 1);
                }
                break;
            case 3:
                if(notes[i].offset >= (unsigned int)currPos)
                    gfx->Draw(graphicsIndex[11], 162 + x, starty);
                gfx->Draw(graphicsIndex[12], 162 + x, endy);
                for(unsigned short a = 0; a < notes[i].value - 8; a++) {
                    if(notes[i].offset + a + 16 >= (unsigned int)currPos)
                        gfx->Draw(graphicsIndex[13], 162 + x, starty - a - 8 - 1);
                }
                break;
            case 4:
                if(notes[i].offset >= (unsigned int)currPos)
                    gfx->Draw(graphicsIndex[8], 190 + x, starty);
                gfx->Draw(graphicsIndex[9], 190 + x, endy);
                for(unsigned short a = 0; a < notes[i].value - 8; a++) {
                    if(notes[i].offset + a + 16 >= (unsigned int)currPos)
                        gfx->Draw(graphicsIndex[10], 190 + x, starty - a - 8 - 1);
                }
                break;
            case 5:
                if(notes[i].offset >= (unsigned int)currPos)
                    gfx->Draw(graphicsIndex[11], 226 + x, starty);
                gfx->Draw(graphicsIndex[12], 226 + x, endy);
                for(unsigned short a = 0; a < notes[i].value - 8; a++) {
                    if(notes[i].offset + a + 16 >= (unsigned int)currPos)
                        gfx->Draw(graphicsIndex[13], 226 + x, starty - a - 8 - 1);
                }
                break;
            case 6:
                if(notes[i].offset >= (unsigned int)currPos)
                    gfx->Draw(graphicsIndex[8], 254 + x, starty);
                gfx->Draw(graphicsIndex[9], 254 + x, endy);
                for(unsigned short a = 0; a < notes[i].value - 8; a++) {
                    if(notes[i].offset + a + 16 >= (unsigned int)currPos)
                        gfx->Draw(graphicsIndex[10], 254 + x, starty - a - 8 - 1);
                }
                break;
            case 7:
                if(notes[i].offset >= (unsigned int)currPos)
                    gfx->Draw(graphicsIndex[5], 0 + x, starty);
                gfx->Draw(graphicsIndex[6], 0 + x, endy);
                for(unsigned short a = 0; a < notes[i].value - 8; a++) {
                    if(notes[i].offset + a + 16 >= (unsigned int)currPos)
                        gfx->Draw(graphicsIndex[7], 0 + x, starty - a - 8 - 1);
                }
                break;
            }
        }
    }
    //std::cout << combo << std::endl;
}

bool Player::IsReadyToQuit()
{
    if(readyToQuit || input.GameShouldClose()) {
        std::cout << "Combo at end: " << combo << std::endl;
        std::cout << "Max combo: " << maxCombo << std::endl;
        std::cout << "ExScore: " << exScore << std::endl;
        std::cout << "Groove Gauge: " << grooveGauge * 2 << std::endl;
        return true;
    }
    return false;
}

void Player::IncreaseCombo() {
    combo++;
    if(combo > maxCombo) {
        maxCombo = combo;
    }
}

void Player::BreakCombo() {
    combo = 0;
}
