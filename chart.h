#ifndef CHART_H
#define CHART_H

enum ChartDifficulties {
    Beginner,
    Normal,
    Hyper,
    Another
};

enum NoteTypes {
    Marker,
    Sample,
    Tempo,
    TimeSigniature,
    Measure,
    Judgement,
    BGA,
    EndOfSong,
    NoteCount
};

struct Note {
    NoteTypes type;
    unsigned int player;
    unsigned int offset;
    unsigned short value;
    unsigned char parameter;
    bool playable;
};

struct Chart {
    ChartDifficulties difficulty;
    unsigned int players;
    std::vector<Note> notes;
};

#endif //CHART_H
