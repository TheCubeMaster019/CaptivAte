#ifndef AUDIOHANDLER_H
#define AUDIOHANDLER_H

#include <SDL/SDL_mixer.h>
#include <vector>

class AudioHandler {
public:
    AudioHandler();
    void SetNumOfSamples(unsigned int num);
    void AddChunk(Mix_Chunk* chunk, unsigned short panning, unsigned short volume); //*does truffle shuffle*
    void PlaySample(unsigned int whatSample);
    ~AudioHandler();
private:
    unsigned int numOfSamples;
    std::vector<Mix_Chunk*> sample_chunks;
    std::vector<unsigned short> pannings;
    std::vector<unsigned short> volumes;
};

#endif //AUDIOHANDLER_H
