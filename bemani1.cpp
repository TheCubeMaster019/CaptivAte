#include "bemani1.h"
#include <cmath>

ONEFile::ONEFile(SDL_RWops* rw, unsigned int offset) {
    file = rw;
    startOffset = offset;
}

Chart ONEFile::OpenChart(ChartDifficulties difficulty, unsigned int players) {
    SDL_RWseek(file, startOffset, SEEK_SET);
    locs = new location_size[12];
    for(unsigned int i = 0; i < 12; i++) {
        SDL_RWread(file, &locs[i].location, sizeof(unsigned int), 1);
        SDL_RWread(file, &locs[i].size, sizeof(unsigned int), 1);
    }
    charts = new Chart[12];
    for(unsigned int i = 0; i < 12; i++) {
        //SANITY CHECKS
        if(locs[i].location < 0x60) {
            continue;
        }
        if(locs[i].size == 0) {
            continue;
        }
        if(locs[i].size % 8 != 0) {
            continue;
        }

        //READ THE CHART
        SDL_RWseek(file, locs[i].location + startOffset, SEEK_SET);
        switch(i) {
        case 0:
            charts[0].difficulty = ChartDifficulties::Hyper;
            charts[0].players = 1;
            break;
        case 1:
            charts[1].difficulty = ChartDifficulties::Normal;
            charts[1].players = 1;
            break;
        case 2:
            charts[2].difficulty = ChartDifficulties::Another;
            charts[2].players = 1;
            break;
        case 6:
            charts[6].difficulty = ChartDifficulties::Hyper;
            charts[6].players = 2;
            break;
        case 7:
            charts[7].difficulty = ChartDifficulties::Normal;
            charts[7].players = 2;
            break;
        case 8:
            charts[8].difficulty = ChartDifficulties::Another;
            charts[8].players = 2;
            break;
        }

        unsigned int offset;
        unsigned char type;
        unsigned char parameter;
        unsigned short value;

        do {
            SDL_RWread(file, &offset, sizeof(unsigned int), 1);
            SDL_RWread(file, &type, sizeof(unsigned char), 1);
            SDL_RWread(file, &parameter, sizeof(unsigned char), 1);
            SDL_RWread(file, &value, sizeof(unsigned short), 1);

            if(offset == 0x7FFFFFFF) {
                continue;
            }

            Note n;
            switch(type) {
            case 0x00:
                n.offset = offset;
                n.type = NoteTypes::Marker;
                n.player = 1;
                n.parameter = parameter; // what key: 0-6 is the 7keys, 7 is TT
                n.value = value; //if not 0, duration of charge note
                break;
            case 0x01:
                n.offset = offset;
                n.type = NoteTypes::Marker;
                n.player = 2;
                n.parameter = parameter; // what key: 0-6 is the 7keys, 7 is TT
                n.value = value; //if not 0, duration of charge note
                break;
            case 0x02:
                n.offset = offset;
                n.type = NoteTypes::Sample;
                n.player = 1;
                n.parameter = parameter; // what key: 0-6 is the 7keys, 7 is TT
                n.value = value; //what sample to play
                break;
            case 0x03:
                n.offset = offset;
                n.type = NoteTypes::Sample;
                n.player = 2;
                n.parameter = parameter; // what key: 0-6 is the 7keys, 7 is TT
                n.value = value; //what sample to play
                break;
            case 0x04:
                n.offset = offset;
                n.type = NoteTypes::Tempo;
                n.value = (unsigned short)std::floor((float)value / parameter);
                break;
            case 0x05:
                n.offset = offset;
                n.type = NoteTypes::TimeSigniature;
                n.parameter = parameter;
                n.value = value;
                break;
            case 0x06:
                n.offset = offset;
                n.type = NoteTypes::EndOfSong;
                n.player = ++parameter;
                break;
            case 0x07:
                n.offset = offset;
                n.type = NoteTypes::BGA;
                n.value = value;
                break;
            case 0x08:
                n.offset = offset;
                n.type = NoteTypes::Judgement;
                n.parameter = parameter;
                n.value = value;
                break;
            case 0x0C:
                n.offset = offset;
                n.type = NoteTypes::Measure;
                n.player = ++parameter;
                break;
            case 0x10:
                n.offset = offset;
                n.type = NoteTypes::NoteCount;
                n.player = ++parameter;
                n.value = value;
                break;
            }
            charts[i].notes.push_back(n);
        } while(offset != 0x7FFFFFFF);
    }
    for(unsigned int i = 0; i < 12; i++) {
        if(charts[i].difficulty == difficulty && charts[i].players == players) {
            return charts[i];
        }
    }
    return charts[0];
}

ONEFile::~ONEFile() {
    if(startOffset == 0) {
        SDL_RWclose(file);
    }
    delete[] locs;
}
