#ifndef GRAPHICSHANDLER_H
#define GRAPHICSHANDLER_H

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <vector>

const unsigned int SCREEN_WIDTH = 1500;
const unsigned int SCREEN_HEIGHT = 800;
const unsigned int SIDE_OFFSET = 50;
const unsigned int BOTTOM_OFFSET = 238;
const int FPS = 120;

class GraphicsHandler {
public:
    GraphicsHandler(unsigned int width, unsigned int height);
    unsigned int AddSurface(const char* path);
    void UpdateScreen();
    void Draw(unsigned int surface, short x, short y);
    ~GraphicsHandler();

private:
    std::vector<SDL_Surface*> surfaces;
    SDL_Surface* windowSurface;
    unsigned int w;
    unsigned int h;
};

#endif //GRAPHICSHANDLER_H
