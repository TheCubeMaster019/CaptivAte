#ifndef PLAYER_H
#define PLAYER_H

#include "inputhandler.h"
#include "chart.h"
#include "graphicshandler.h"
#include "audiohandler.h"

const int JUDGE_FPS_CONST = 30;

class Player {
public:
    Player(Chart c, GraphicsHandler* g, AudioHandler* a);
    void Update();
    void Render(int x, int y);
    bool IsReadyToQuit();
private:
    void IncreaseCombo();
    void BreakCombo();
    unsigned short grooveGauge;
    unsigned char grooveCounter;
    unsigned int exScore;
    unsigned int numberOfNotes;
    unsigned int combo;
    unsigned int maxCombo;
    InputHandler input;
    Chart chart;
    unsigned int playback_sample[16] = {0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF};
    short judgements[8] = {-16 * 1000/JUDGE_FPS_CONST - (500 / JUDGE_FPS_CONST), -6 * 1000/JUDGE_FPS_CONST - (500 / JUDGE_FPS_CONST), -1 * 1000/JUDGE_FPS_CONST - (500 / JUDGE_FPS_CONST), 3 * 1000/JUDGE_FPS_CONST - (500 / JUDGE_FPS_CONST), 8 * 1000/JUDGE_FPS_CONST - (500 / JUDGE_FPS_CONST), 18 * 1000/JUDGE_FPS_CONST - (500 / JUDGE_FPS_CONST)};
    int currPos;
    GraphicsHandler* gfx;
    AudioHandler* aud;
    bool readyToQuit;
    unsigned int graphicsIndex[14];
};

#endif //PLAYER_H
