#include "audiohandler.h"

AudioHandler::AudioHandler() {
    Mix_Init(MIX_INIT_MP3 | MIX_INIT_OGG | MIX_INIT_FLAC);
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 2048);
}

void AudioHandler::SetNumOfSamples(unsigned int num)
{
    numOfSamples = num;
}

void AudioHandler::AddChunk(Mix_Chunk* chunk, unsigned short panning, unsigned short volume) {
    if(sample_chunks.size() >= numOfSamples) {
        return;
    }
    sample_chunks.push_back(chunk);
    pannings.push_back(panning);
    volumes.push_back(volume);
}

void AudioHandler::PlaySample(unsigned int whatSample) {
    if(whatSample > numOfSamples || whatSample == 0) {
        return;
    }
    whatSample--;
    Mix_HaltChannel(whatSample);
    unsigned int pan = (unsigned int)((float)pannings[whatSample] / 0x80 * 0xFF);
    Mix_SetPanning(whatSample, (unsigned char)(pan * ((float)volumes[whatSample] / 255)), (unsigned char)((0xFF - pan) * ((float)volumes[whatSample] / 255)));
    Mix_PlayChannel(whatSample, sample_chunks[whatSample], SDL_FALSE);
}

AudioHandler::~AudioHandler()
{
    for(unsigned int i = 0; i < numOfSamples; i++) {
        Mix_FreeChunk(sample_chunks[i]);
    }

    Mix_CloseAudio();
}
