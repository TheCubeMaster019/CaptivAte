#include "ifsfile.h"

void flipInt(unsigned int* i) {
    unsigned int o = 0;
    o += (*i & 0xFF000000) >> 24;
    o += (*i & 0x00FF0000) >> 8;
    o += (*i & 0x0000FF00) << 8;
    o += (*i & 0x000000FF) << 24;
    *i = o;
}

IFSFile::IFSFile(SDL_RWops* rw) {
    file = rw;
    SDL_RWseek(file, 16, SEEK_CUR);
    SDL_RWread(file, &headerSize, sizeof(unsigned int), 1);
    flipInt(&headerSize);
    SDL_RWseek(file, 20, SEEK_CUR);
    unsigned int A = 0 , B = 0;
    SDL_RWread(file, &A, sizeof(unsigned int), 1);
    flipInt(&A);
    SDL_RWseek(file, A, SEEK_CUR);
    SDL_RWread(file, &B, sizeof(unsigned int), 1);
    flipInt(&B);
    SDL_RWseek(file, 52, SEEK_CUR);
    SDL_RWread(file, &iidxSize, sizeof(unsigned int), 1);
    flipInt(&iidxSize);
}

SDL_RWops* IFSFile::GetRW() {
    return file;
}

unsigned int IFSFile::Get2DXOffset() {
    return headerSize;
}

unsigned int IFSFile::Get1Offset() {
    return headerSize + iidxSize;
}

IFSFile::~IFSFile() {
    SDL_RWclose(file);
}
