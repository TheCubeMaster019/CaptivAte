#ifndef TIMEHANDLER_H
#define TIMEHANDLER_H

#include <chrono>

class Timer {
public:
    Timer(std::chrono::microseconds resolution);
    void Start();
    bool HasTicked();
    void Sleep();
private:
    std::chrono::time_point<std::chrono::steady_clock> prevTime;
    std::chrono::time_point<std::chrono::steady_clock> currTime;
    std::chrono::microseconds duration;
};

#endif //TIMEHANDLER_H
